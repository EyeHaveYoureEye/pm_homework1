package pm.academy.hw1;

public class HomeWorkClass {
    public int getPrimitiveFive() {
        int five = 5;
        return five;
    }

    public Integer getMinInteger() {
        return 0x80000000;
    }

    public Integer getMaxInteger() {
        return 0x7fffffff;
    }

    public Long getMinLong() {
        return -9223372036854775808L;
    }

    public Long getMaxLong() {
        return 9223372036854775807L;
    }

    public String writeToString() {
        int i = 1;
        String name=String.valueOf(i);
        i++;
        for (;i<=10;i++) {
            name+=String.valueOf(i);

        }
        return name;
    }

    public boolean checkForNegativeValue(int number) {
        if(number<0)
            return true;
        else
            return false;

    }

    public int getWeekday(Weekdays weekday) {
        if(weekday == null)
            return -1;
        int day;
        switch (weekday)
        {
            case MONDAY:
                day=weekday.getNumber();
                break;
            case TUESDAY:
                day=weekday.getNumber();
                break;
            case WEDNESDAY:
                day=weekday.getNumber();
                break;
            case THURSDAY:
                day=weekday.getNumber();
                break;
            case FRIDAY:
                day=weekday.getNumber();
                break;
            case SATURDAY:
                day=weekday.getNumber();
                break;
            case SUNDAY:
                day=weekday.getNumber();
                break;
            default:
                day=-1;
                break;
        }
        return day;
    }

    public int getFactorial(int val) {
        if (val<0)
            return -1;
        else {
            int factorial = 1;
            for (int i = 1; i <= val; i++) {
                factorial *= i;
            }
            return factorial;
        }
    }

    public int findMaxValue(int a, int b) {
        if (a>b)
            return a;
        else if (a<b)
            return b;
        else
            return a;
    }
}
