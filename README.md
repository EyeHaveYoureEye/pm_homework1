# Home Work 1

0. Clone the project to your local machine
1. Open HomeWorkClassTest using alt+shift+N shortcut
2. Read comments in tests carefully
3. Run test with ctrl+shift+F10
4. Your goal is to make all tests "green" and push your project to
  feature branch "feature/[your first and last name]"
5. Example of branch name: feature/artur.ashyrov

Deadline:
Mon, 7 of June, 12:00 